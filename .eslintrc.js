module.exports = {
  extends: "airbnb",

  env: {
    browser: true,
    node: true,
  },

  globals: {
    jest: true,
    it: true,
    expect: true,
    describe: true,
    beforeEach: true,
  },

  plugins: [
    "react", "import", "jsx-a11y",
  ],

  rules: {
    "comma-dangle": ["error", {
      arrays: "always-multiline",
      objects: "always-multiline",
      imports: "always-multiline",
      exports: "always-multiline",
      functions: "ignore",
    }],
    "function-paren-newline": ["error", "consistent"],
    "object-curly-newline": [
      "error", { ObjectExpression: { multiline: true, consistent: true } },
    ],
    "react/jsx-filename-extension": [
      1, {
        extensions: [".js", ".jsx"],
      },
    ],
    quotes: [1, "double"],
  },

  settings: {
    "import/resolver": {
      node: {
        moduleDirectory: ["node_modules", "src", ""],
      },
    },
  },
};
