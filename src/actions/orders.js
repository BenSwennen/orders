import { get, save } from "../api/orders";

export const ORDERS_GET = "ORDERS_GET";
export const ORDER_SAVE = "ORDER_SAVE";

export const getOrders = () => dispatch => (
  dispatch({
    type: ORDERS_GET,
    payload: get(),
  })
);

export const saveOrder = id => (dispatch, getState) => {
  const order = getState().orders[id];
  const { items } = getState();
  const orderWithItems = { ...order, items: order.items.map(i => items[i]) };

  dispatch({
    type: ORDER_SAVE,
    payload: save(orderWithItems),
  }).then((({ value }) => {
    console.log("Order was updated!", value); // eslint-disable-line
    alert(`Order was updated!`); // eslint-disable-line

    return value;
  }));
};
