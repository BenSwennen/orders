import apiGetProducts from "../api/products";

export const PRODUCTS_GET = "PRODUCTS_GET";
export const PRODUCT_ADD = "PRODUCT_ADD";

export const getProducts = () => dispatch => (
  dispatch({
    type: PRODUCTS_GET,
    payload: apiGetProducts(),
  })
);

export const addProduct = id => (dispatch, getState) => {
  const product = getState().products[id];
  dispatch({
    type: PRODUCT_ADD,
    payload: product,
  });

  alert(`Product ${id} has been added to order!`); // eslint-disable-line
};
