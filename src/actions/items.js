export const ITEM_QUANTITY_UPDATE = "ITEM_QUANTITY_UPDATE";

export const updateQuantity = (id, quantity) => dispatch => (
  dispatch({
    type: ITEM_QUANTITY_UPDATE,
    meta: { id },
    payload: { quantity },
  })
);
