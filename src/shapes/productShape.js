import { number, string } from "prop-types";

export default {
  id: string.isRequired,
  description: string.isRequired,
  price: number.isRequired,
};
