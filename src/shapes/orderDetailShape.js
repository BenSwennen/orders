import { arrayOf, number, shape, string } from "prop-types";
import { itemShape } from ".";

export default {
  id: string.isRequired,
  items: arrayOf(shape(itemShape).isRequired).isRequired,
  total: number.isRequired,
};
