import { number, string } from "prop-types";

export default {
  id: string.isRequired,
  total: number.isRequired,
};
