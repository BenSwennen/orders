import { func, number, oneOfType, string } from "prop-types";

export default {
  id: string.isRequired,
  quantity: oneOfType([number, string]).isRequired,
  unitPrice: number.isRequired,
  updateQuantity: func.isRequired,
};
