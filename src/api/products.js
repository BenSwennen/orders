import { API_URL } from "../constants";
import { jsonFetch } from "../utils";

const get = () => (
  jsonFetch(`${API_URL}/products.json`, { method: "GET" })
);

export default get;
