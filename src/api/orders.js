import { API_URL } from "../constants";
import { jsonFetch } from "../utils";

const get = () => (
  jsonFetch(`${API_URL}/orders.json`, { method: "GET" })
);

const save = order => (
  // TODO: replace me with actual API call
  new Promise(resolve => (setTimeout(() => {
    resolve(order);
  }, 200)))
);

export { get, save };
