import React from "react";
import PropTypes from "prop-types";

const Order = ({ id, total, showDetail }) => (
  <div className="ListItem">
    <h3 className="title">Order {id}</h3>
    <div>
      <span>Total: </span>
      <strong>€{total.toFixed(2)}</strong>
    </div>
    <button className="right" onClick={() => showDetail(id)}>Show detail</button>
  </div>
);

Order.propTypes = {
  id: PropTypes.string.isRequired,
  total: PropTypes.number.isRequired,
  showDetail: PropTypes.func.isRequired,
};

export default Order;
