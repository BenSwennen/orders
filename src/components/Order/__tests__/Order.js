import React from "react";
import renderer from "react-test-renderer";

import Order from "../";

describe("<Order />", () => {
  it("renders correctly", () => {
    const component = renderer.create(
      <Order id="1" total={92.21} />
    ).toJSON();
    expect(component).toMatchSnapshot();
  });
});
