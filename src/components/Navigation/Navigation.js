import React from "react";
import { NavLink } from "react-router-dom";

import "./Navigation.css";

const Navigation = () => (
  <nav className="Navigation">
    <NavLink to="/products">Products</NavLink>
    <NavLink to="/orders">My Orders</NavLink>
  </nav>
);

export default Navigation;
