import React from "react";
import PropTypes from "prop-types";

import OrderItem from "../OrderItem";

const OrderDetails = ({ back, id, items, save, updateQuantity }) => (
  <div>
    <h3>Order {id}</h3>
    {items.map(item => (
      <OrderItem
        {...item}
        updateQuantity={updateQuantity}
        key={item.id}
      />
    ))}
    <br />
    <button onClick={back}>&laquo; Go back</button>
    <button onClick={() => save(id)}>Update order</button>
  </div>
);

OrderDetails.propTypes = {
  id: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    quantity: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    unitPrice: PropTypes.number.isRequired,
  }).isRequired),
  back: PropTypes.func.isRequired,
  save: PropTypes.func.isRequired,
  updateQuantity: PropTypes.func.isRequired,
};

OrderDetails.defaultProps = {
  items: [],
};


export default OrderDetails;
