import React from "react";
import PropTypes from "prop-types";

import { orderShape } from "../../shapes";
import Order from "../Order";

const OrdersList = ({ orders, showDetail }) => (
  <div>
    <h2>My Orders</h2>
    {orders.map(o => <Order key={o.id} {...o} showDetail={showDetail} />)}
  </div>
);

OrdersList.propTypes = {
  orders: PropTypes.arrayOf(PropTypes.shape(orderShape).isRequired).isRequired,
  showDetail: PropTypes.func.isRequired,
};

export default OrdersList;
