import React from "react";
import { itemShape } from "../../shapes";

import "./OrderItem.css";

const OrderItem = ({ id, quantity, unitPrice, total, updateQuantity }) => {
  const increment = () => updateQuantity(id, quantity + 1);
  const decrement = () => updateQuantity(id, quantity - 1);
  const update = evt => updateQuantity(id, evt.target.value ? parseInt(evt.target.value, 10) : "");

  return (
    <div className="OrderItem">
      <div className="title">{id}</div>
      <div>€{unitPrice}</div>
      <div>
        <button onClick={increment}>+</button>
        <input type="number" value={quantity} onChange={update} />
        <button onClick={decrement}>-</button>
      </div>
      <div><strong>€{total.toFixed(2)}</strong></div>
    </div>
  );
};

OrderItem.propTypes = itemShape;

export default OrderItem;
