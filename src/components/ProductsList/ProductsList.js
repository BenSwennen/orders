import React from "react";
import PropTypes from "prop-types";

import { productShape } from "../../shapes";
import Product from "../Product";

const ProductsList = ({ addProduct, products }) => (
  <div>
    <h2>Products</h2>
    {products.map(p => <Product key={p.id} {...p} addProduct={addProduct} />)}
  </div>
);

ProductsList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape(productShape)).isRequired,
  addProduct: PropTypes.func.isRequired,
};

export default ProductsList;
