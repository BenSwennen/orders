import React from "react";
import { Redirect } from "react-router-dom";

const NoMatchRoute = () => (
  <Redirect to="/orders" />
);

export default NoMatchRoute;
