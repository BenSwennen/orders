import React from "react";
import PropTypes from "prop-types";

import { productShape } from "../../shapes";

const Product = ({ id, description, price }) => (
  <div className="ListItem">
    <h3 className="title">{id}</h3>
    <div>{description}, €{price}</div>
    {
      // <button className="right" onClick={() => addProduct(id)}>Order</button>
    }
  </div>
);

Product.propTypes = {
  ...productShape,
  addProduct: PropTypes.func.isRequired,
};

export default Product;
