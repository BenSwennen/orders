import { connect } from "react-redux";

import { addProduct } from "../actions/products";
import { getProducts } from "../selectors";
import ProductsList from "../components/ProductsList";

const mapStateToProps = state => ({
  products: getProducts(state),
});

const mapDispatchToProps = dispatch => ({
  addProduct: id => dispatch(addProduct(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductsList);
