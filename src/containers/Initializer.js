import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { getOrders } from "../actions/orders";
import { getProducts } from "../actions/products";

class Initializer extends React.Component {
  componentWillMount() {
    this.props.getOrders();
    this.props.getProducts();
  }

  render() {
    return this.props.children;
  }
}

Initializer.propTypes = {
  children: PropTypes.node.isRequired,
  getOrders: PropTypes.func.isRequired,
  getProducts: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  getOrders: () => dispatch(getOrders()),
  getProducts: () => dispatch(getProducts()),
});


// TODO: use location? https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/guides/blocked-updates.md
export default withRouter(connect(null, mapDispatchToProps)(Initializer));
