import { connect } from "react-redux";
import { push } from "react-router-redux";

import { getOrders } from "../selectors";
import OrdersList from "../components/OrdersList";

const mapStateToProps = state => ({
  orders: getOrders(state),
});

const mapDispatchToProps = dispatch => ({
  showDetail: id => dispatch(push(`/orders/${id}`)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrdersList);
