import { connect } from "react-redux";
import { goBack } from "react-router-redux";

import { updateQuantity } from "../actions/items";
import { saveOrder } from "../actions/orders";
import { getOrder } from "../selectors";
import OrderDetails from "../components/OrderDetails";

const mapStateToProps = (state, { match }) => ({
  ...getOrder(state, match.params.id),
});

const mapDispatchToProps = dispatch => ({
  back: () => dispatch(goBack()),
  save: order => dispatch(saveOrder(order)),
  updateQuantity: (id, quantity) => dispatch(updateQuantity(id, quantity)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetails);
