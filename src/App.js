import React from "react";
import { ConnectedRouter } from "react-router-redux";
import { Route, Switch } from "react-router-dom";

import { Navigation, NoMatchRoute } from "./components";
import { Initializer, OrderDetail, Orders, Products } from "./containers";
import history from "./utils/history";
import "./App.css";

const App = () => (
  <ConnectedRouter history={history}>
    <div className="App">
      <Initializer>
        <Navigation />
        <Switch>
          <Route exact path="/products" component={Products} />
          <Route exact path="/orders" component={Orders} />
          <Route exact path="/orders/:id" component={OrderDetail} />
          <Route component={NoMatchRoute} />
        </Switch>
      </Initializer>
    </div>
  </ConnectedRouter>
);

export default App;
