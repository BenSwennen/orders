import { normalize } from "normalizr";
import { handleActions } from "redux-actions";

import { orderListSchema } from "../schemas";

export default handleActions({
  ORDERS_GET: {
    FULFILLED: (_orders, { payload }) => normalize(payload, orderListSchema).entities.orders,
  },
}, {}, { namespace: "_" });
