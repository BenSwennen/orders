import { normalize } from "normalizr";
import { handleActions } from "redux-actions";

import { orderListSchema } from "../schemas";

export default handleActions({
  ORDERS_GET: {
    FULFILLED: (_items, { payload }) => (
      normalize(payload, orderListSchema).entities.items
    ),
  },
  PRODUCT_ADD: (items, { payload }) => ({
    ...items, [payload.id]: payload,
  }),
  ITEM_QUANTITY_UPDATE: (items, { meta, payload }) => ({
    ...items, [meta.id]: { ...items[meta.id], quantity: payload.quantity },
  }),
}, {}, { namespace: "_" });
