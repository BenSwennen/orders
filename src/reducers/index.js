import { combineReducers } from "redux";
import items from "./items";
import orders from "./orders";
import products from "./products";

export default combineReducers({ items, orders, products });
