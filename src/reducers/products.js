import { normalize } from "normalizr";
import { handleActions } from "redux-actions";

import { productListSchema } from "../schemas";

export default handleActions({
  PRODUCTS_GET: {
    FULFILLED: (state, { payload }) => normalize(payload, productListSchema).entities.products,
  },
}, {}, { namespace: "_" });
