import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import { routerMiddleware } from "react-router-redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import promiseMiddleware from "redux-promise-middleware";

import history from "./utils/history";
import rootReducer from "./reducers";

const loggerMiddleware = createLogger();
const middleware = [
  thunk,
  promiseMiddleware(),
  routerMiddleware(history),
  loggerMiddleware,
];

const defaultState = { orders: {}, products: {} };

export default createStore(
  rootReducer,
  defaultState,
  composeWithDevTools(applyMiddleware(...middleware)),
);
