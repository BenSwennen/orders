import fetch from "isomorphic-fetch";
import toCamelCase from "camelcase-keys";
import toKebabCase from "dashify";

export const convertToJson = response => (
  response.json()
    .then(json => ({ ok: response.ok, body: json }))
    .then(json => toCamelCase(json, { deep: true }))
    .then((json) => {
      if (!json.ok) {
        const error = { success: false, status: response.status };
        throw error;
      }
      return json.body;
    })
);


const jsonFetch = (url, options = {}) => {
  const headers = {
    ...options.headers,
    Accept: "application/json",
    "Content-Type": "application/json",
  };

  const body = options.method !== "GET"
    ? JSON.stringify(toKebabCase(options.body))
    : options.body;

  return fetch(url, { ...options, headers, body })
    .then(response => convertToJson(response));
};

export default jsonFetch;
