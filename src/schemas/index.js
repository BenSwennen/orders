import { orderSchema, orderListSchema } from "./order";
import { productSchema, productListSchema } from "./product";

export {
  orderSchema,
  orderListSchema,
  productSchema,
  productListSchema,
};
