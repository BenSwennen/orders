import { schema } from "normalizr";
import { omit } from "lodash";
import { productSchema } from "./product";

const itemSchema = new schema.Entity("items", { product: productSchema }, {
  idAttribute: "productId",
  processStrategy: value => ({
    ...omit(value, "productId", "total"),
    id: value.productId,
    product: value.productId,
    unitPrice: parseFloat(value.unitPrice),
    quantity: parseFloat(value.quantity),
  }),
});

const itemListSchema = [itemSchema];

export { itemSchema, itemListSchema };
