import { schema } from "normalizr";
import { omit } from "lodash";
import { itemSchema } from "./item";

const orderSchema = new schema.Entity("orders", { items: [itemSchema] }, {
  processStrategy: value => ({ ...omit(value, "total") }),
});

const orderListSchema = [orderSchema];

export { orderSchema, orderListSchema };
