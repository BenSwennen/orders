import { schema } from "normalizr";

const productSchema = new schema.Entity("products", {}, {
  processStrategy: value => ({ ...value, price: parseFloat(value.price) }),
});

const productListSchema = [productSchema];

export { productSchema, productListSchema };
