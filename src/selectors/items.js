import { createSelector } from "reselect";

const itemsSelector = state => state.items;

export default createSelector(
  [itemsSelector],
  items => (Object.keys(items).map(id => ({
    ...items[id],
    total: items[id].quantity * items[id].unitPrice,
  })).reduce((obj, item) => ({ ...obj, [item.id]: item }), {}))
);
