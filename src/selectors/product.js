import { createSelector } from "reselect";

export const productsSelector = state => state.products;

const productsArraySelector = createSelector(
  [productsSelector],
  products => Object.keys(products).map(id => products[id]),
);

export default createSelector(
  [productsArraySelector],
  products => products
);
