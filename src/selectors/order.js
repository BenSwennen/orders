import { createSelector } from "reselect";
import itemsSelector from "./items";

const ordersSelector = state => state.orders;
const orderSelector = (state, id) => state.orders[id];

const ordersArraySelector = createSelector(
  [ordersSelector],
  orders => Object.keys(orders).map(id => orders[id]),
);

const getOrders = createSelector(
  [ordersArraySelector, itemsSelector],
  (orders, items) => orders.reduce((arr, obj) => ([
    ...arr,
    { ...obj, total: obj.items.map(id => items[id]).reduce((sum, item) => (sum + item.total), 0) },
  ]), []),
);

const getOrder = createSelector(
  [orderSelector, itemsSelector],
  (order, items) => {
    if (!order) return null;

    return ({
      ...order,
      items: order.items.map(id => items[id]),
      total: order.items.map(id => items[id]).reduce((sum, item) => (sum + item.total), 0),
    });
  }
);

export { getOrder };
export default getOrders;
