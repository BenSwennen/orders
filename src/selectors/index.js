import getOrders, { getOrder } from "./order";
import getProducts from "./product";

export { getOrder, getOrders, getProducts };
